import numpy as np
import sympy
from sympy import *
from sympy.printing.str import StrPrinter
th = Symbol('th')
ph = Symbol('ph')
ps = Symbol('ps')
al = Symbol('al')
bbt = Symbol('bt')
bt = bbt
gm = Symbol('gm')
Rx = Matrix([[1, 0, 0], [0, cos(al), -sin(al)], [0, sin(al), cos(al)]])
Ry = Matrix([[cos(bt), 0, sin(bt)], [0, 1, 0], [-sin(bt), 0, cos(bt)]])
Rz = Matrix([[cos(gm), -sin(gm), 0], [sin(gm), cos(gm), 0], [0, 0, 1]])
Rza = Matrix([[cos(al), -sin(al), 0], [sin(al), cos(al), 0], [0, 0, 1]])

printer = StrPrinter()

rr = Rx*Ry*Rz
print('--- XYZ\n', rr.table(printer))

rr = Rz*Ry*Rza
print('--- ZYZ\n', rr.table(printer))

rr = Ry*Rz*Rx
print('--- YZX\n', rr.table(printer))
