"""This module provides ParaView filters for working with frame fields
based on work by N. Ray and D. Sokolov. These are implemented using
VTKPythonAlgorithmBase and make extensive use of NumPy for performing
unconstrained quadratic optimization."""

from paraview.util.vtkAlgorithm import *
import numpy as np

def AngleFromVector(vec):
    return np.arctan2(vec[1], vec[0])

@smproxy.filter(label='Planar Feature Points')
@smhint.xml("""
    <ShowInMenu category="Frame field"/>
    <RepresentationType port="0" view="RenderView" type="Surface" />
""")
@smproperty.input(name="Surface mesh", port_index=0)
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=False)
class PlanarFeaturePoints(VTKPythonAlgorithmBase):
    """Compute feature points (points on boundary edges and sharp corners)
    used to constrain frame fields."""
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkPolyData")
        self.FeatureAngle = 30
        self.CosFeatureAngle = np.cos(self.FeatureAngle * np.pi / 180.0)

    @smproperty.doublevector(name="FeatureAngle", default_values=[30])
    @smdomain.doublerange(min=0, max=180)
    def SetFeatureAngle(self, angle):
        if angle != self.FeatureAngle:
            self.FeatureAngle = angle
            self.CosFeatureAngle = np.cos(angle * np.pi / 180.0)
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetFeatureAngle(self):
        return self.FeatureAngle

    def ProcessNormal(self, ptId, otherId, ptNorm, edgeDir, endpoints, avgBdyNorm, cornerNorm, counts):
        tangentNorm = np.cross(edgeDir, ptNorm)
        tangentNorm = tangentNorm / np.linalg.norm(tangentNorm)
        if ptId in endpoints:
            # Test whether this is a corner
            # If not a corner, average outward pointing normal using existing entry
            # If a corner, compute orthogonal vector to existing entry for constraint
            endpoints[ptId].insert(1, otherId)
            bdyNorm = np.array(avgBdyNorm.GetTuple(ptId))
            if np.dot(tangentNorm, bdyNorm) < self.CosFeatureAngle:
                # Normals are too far apart, treat as a corner
                cornerNorm.SetTuple(ptId, tangentNorm)
                counts[1] += 1
            else:
                bdyNorm = 0.5 * (bdyNorm + tangentNorm)
                bdyNorm = bdyNorm / np.linalg.norm(bdyNorm)
                avgBdyNorm.SetTuple(ptId, bdyNorm)
        else:
            endpoints[ptId] = [otherId,]
            avgBdyNorm.SetTuple(ptId, tangentNorm)
            counts[0] += 1

    def ComputeFeatures(self, pdIn):
        from vtkmodules.vtkCommonCore import vtkDoubleArray
        from vtkmodules.vtkFiltersCore import vtkPolyDataNormals as SurfaceNormals
        from vtkmodules.vtkFiltersCore import vtkFeatureEdges as FeatureEdges
        from vtkmodules.vtkFiltersCore import vtkTriangleFilter as TriangleFilter
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton

        tf = TriangleFilter()
        tf.SetInputDataObject(0, pdIn)

        # I. Compute surface normals (without splitting) so we can
        #    determine which direction is outward-pointing.

        sn = SurfaceNormals()
        sn.SetInputConnection(tf.GetOutputPort())
        sn.AutoOrientNormalsOn()
        sn.ComputePointNormalsOn()
        sn.ComputeCellNormalsOff()
        sn.SplittingOff() # Otherwise point IDs would not match edge connectivity output below.
        sn.Update()
        normals = vton(sn.GetOutputDataObject(0).GetPointData().GetNormals())

        # II. Find boundary and feature edges, computing an averaged normal
        #     vector for each. For points on sharp corners where 2 edges
        #     meet, we add a second vector in an additional output array.
        #     For vertices interior to the mesh, both vector arrays will
        #     have a zero-length entry.

        fe = FeatureEdges()
        fe.SetInputConnection(tf.GetOutputPort())
        fe.GeneratePedigreeIdsOn()
        fe.BoundaryEdgesOn()
        fe.FeatureEdgesOn()
        fe.ColoringOn() # Generate 'Edge Types' cell-data
        fe.SetFeatureAngle(self.FeatureAngle)
        fe.Update()

        edgeData = fe.GetOutputDataObject(0)
        lines = edgeData.GetLines()
        lconn = vton(lines.GetConnectivityArray())
        feat = np.reshape(lconn, (int(lconn.shape[0]/2), 2))
        bds = edgeData.GetBounds()
        pedId = vton(edgeData.GetPointData().GetPedigreeIds())
        surfData = sn.GetOutputDataObject(0)
        if bds[4] == bds[5]:
            # We are in the x-y plane, so use world coordinates
            pts = vton(surfData.GetPoints().GetData())[:,0:2]
        else:
            # Data is non-planar (certainly not in x-y plane), so
            # we require 2-D texture coordinates
            pts = vton(surfData.GetPointData().GetTCoords())[:,0:2]

        # Now make a dict from each feature-edge endpoint to all the other
        # endpoints along feature edges. This, plus the outward-pointing
        # *surface* normal out of the plane let us determine an outward-pointing
        # *tangent* normal in the plane. (Some modification will be required for
        # feature edges as opposed to boundary edges.)
        endpoints = dict()
        avgBdyNorm = vtkDoubleArray()
        avgBdyNorm.SetName('BoundaryNormal')
        avgBdyNorm.SetNumberOfComponents(3)
        avgBdyNorm.SetNumberOfTuples(pts.shape[0])
        cornerNorm = vtkDoubleArray()
        cornerNorm.SetName('CornerNormal')
        cornerNorm.SetNumberOfComponents(3)
        cornerNorm.SetNumberOfTuples(pts.shape[0])
        for cc in range(3):
            avgBdyNorm.FillComponent(cc, 0.0);
            cornerNorm.FillComponent(cc, 0.0);
        counts = [0, 0]
        for edge in feat:
            # Consider each endpoint of the edge.
            e0 = edge[0]
            e1 = edge[1]
            edir = pts[pedId[e1],:] - pts[pedId[e0],:]
            edir = np.hstack((edir / np.linalg.norm(edir), 0))
            e0n = normals[pedId[e0],:]
            e1n = normals[pedId[e1],:]
            self.ProcessNormal(pedId[e0], pedId[e1], e0n, edir, endpoints, avgBdyNorm, cornerNorm, counts)
            self.ProcessNormal(pedId[e1], pedId[e0], e1n, edir, endpoints, avgBdyNorm, cornerNorm, counts)
        return avgBdyNorm, cornerNorm, counts

    def RequestData(self, request, inInfoVec, outInfoVec):
        from vtkmodules.vtkCommonCore import vtkIdTypeArray
        from vtkmodules.vtkCommonDataModel import vtkTable, vtkDataSet, vtkPolyData
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        from vtkmodules.util.numpy_support import numpy_to_vtk as ntov

        pdIn = vtkPolyData.GetData(inInfoVec[0], 0)
        output = vtkPolyData.GetData(outInfoVec, 0)
        output.ShallowCopy(pdIn)

        bdyVertEdgeNormals, bdyVertCornerNormals, counts = self.ComputeFeatures(pdIn)
        # print('Feat vert counts %d %d = %d' % (counts[0], counts[1], counts[0] + counts[1]))
        featVertCounts = vtkIdTypeArray()
        featVertCounts.SetName('Feature Vertex Counts')
        featVertCounts.InsertNextValue(counts[0] + counts[1])
        output.GetPointData().AddArray(bdyVertEdgeNormals)
        output.GetPointData().AddArray(bdyVertCornerNormals)
        output.GetFieldData().AddArray(featVertCounts)
        return 1

@smproxy.filter(label='Planar Frame Field')
@smhint.xml("""
    <ShowInMenu category="Frame field"/>
    <RepresentationType port="0" view="RenderView" type="Surface" />
""")
@smproperty.input(name="Triangle mesh", port_index=0)
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=False)
class PlanarFrameField(VTKPythonAlgorithmBase):
    """Compute the frame field of a planar triangular mesh."""
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkPolyData")
        self.FeatureAngle = 30
        self.Lambda = 100
        self.SmoothingIterations = 8

    @smproperty.doublevector(name="FeatureAngle", default_values=[30])
    @smdomain.doublerange(min=0, max=180)
    def SetFeatureAngle(self, angle):
        if angle != self.FeatureAngle:
            self.FeatureAngle = angle
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetFeatureAngle(self):
        return self.FeatureAngle

    @smproperty.doublevector(name="Lambda", default_values=[100])
    @smdomain.doublerange(min=0, max=1000)
    def SetLambda(self, lmbda):
        if lmbda != self.Lambda:
            self.Lambda = lmbda
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetLambda(self):
        return self.Lambda

    @smproperty.intvector(name="SmoothingIterations", default_values=[8])
    @smdomain.intrange(min=0, max=1000)
    def SetSmoothingIterations(self, smoothIt):
        if smoothIt != self.SmoothingIterations:
            self.SmoothingIterations = smoothIt
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetSmoothingIterations(self):
        return self.SmoothingIterations

    def RequestData(self, request, inInfoVec, outInfoVec):
        from vtkmodules.vtkCommonDataModel import vtkPolyData
        # from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        # from vtkmodules.util.numpy_support import numpy_to_vtk as ntov

        pdIn = vtkPolyData.GetData(inInfoVec[0], 0)
        output = vtkPolyData.GetData(outInfoVec, 0)
        output.ShallowCopy(pdIn)

        edges, bdyVertConstraints = self.ComputeFeatures(pdIn)
        ai = self.ComputeFrameField(pdIn, bdyVertConstraints, edges)
        v0, v1 = self.FrameFieldVectors(ai)
        output.GetPointData().AddArray(v0)
        output.GetPointData().AddArray(v1)
        return 1

    def ComputeFeatures(self, pdIn):
        from vtkmodules.vtkFiltersExtraction import vtkExtractEdges as ExtractEdges
        from vtkmodules.vtkFiltersCore import vtkTriangleFilter as TriangleFilter
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton

        tf = TriangleFilter()
        tf.SetInputDataObject(0, pdIn)

        # I. Extract all edges of the mesh
        #    These are used to compute the strain energy between adjacent frame fields.
        #    The strain energy is the quantity minimized to obtain a smooth frame field.
        ee = ExtractEdges()
        ee.SetInputConnection(tf.GetOutputPort())
        ee.Update()

        edgeData = ee.GetOutputDataObject(0)
        lines = edgeData.GetLines()
        lconn = vton(lines.GetConnectivityArray())
        edges = np.reshape(lconn, (int(lconn.shape[0]/2), 2))

        # II. Find boundary edges (and feature edges) and add penalty terms
        #     to involved vertices that attempt to preserve normals (along
        #     edge-like regions).
        fp = PlanarFeaturePoints()
        fp.SetFeatureAngle(self.FeatureAngle)
        fp.SetInputConnection(tf.GetOutputPort())
        fp.Update()

        nf = fp.GetOutputDataObject(0).GetFieldData().GetArray('Feature Vertex Counts').GetValue(0)
        bdyVertConstraints = np.zeros((nf, 2))
        fpdata = fp.GetOutputDataObject(0).GetPointData()
        avgBdyNorm = vton(fpdata.GetArray('BoundaryNormal'))
        cornerNorm = vton(fpdata.GetArray('CornerNormal'))
        ii = 0
        for jj in range(avgBdyNorm.shape[0]):
            bnt = avgBdyNorm[jj,:]
            if np.linalg.norm(bnt) > 0.0:
                bdyVertConstraints[ii, 0] = jj
                bdyVertConstraints[ii, 1] = AngleFromVector(bnt)
                print('  Add constraint %2d for node %2d th %g vec %s' % (ii, jj, AngleFromVector(bnt), str(bnt)))
                ii += 1
            cnt = cornerNorm[jj,:]
            if np.linalg.norm(cnt) > 0.0:
                bdyVertConstraints[ii, 0] = jj
                bdyVertConstraints[ii, 1] = AngleFromVector(cnt)
                print('  Add constraint %2d for node %2d th %g vec %s' % (ii, jj, AngleFromVector(cnt), str(cnt)))
                ii += 1

        return edges, bdyVertConstraints

    def ComputeMatrix(self, numVerts, edges, bdyVertConstraints, ai = None):
        numEdges = edges.shape[0]
        numBdyVerts = bdyVertConstraints.shape[0]
        # numVerts = np.max(edges) - np.min(edges) + 1 # 4
        numUnknown = 2 * numVerts # - 2 *numBdyVerts) ? No
        numSmooth = 0
        if type(ai) != type(None):
            numSmooth = ai.shape[0]

        A = np.zeros((2 * numEdges + 2 * numBdyVerts + numSmooth, numUnknown))
        b = np.zeros(2 * numEdges + 2 * numBdyVerts + numSmooth)

        rpi = np.sqrt(np.pi)
        for ei in range(numEdges):
            edge = edges[ei,:]
            ii = edge[0]
            jj = edge[1]
            # Eqn 1
            A[2 * ei,2*ii] += rpi
            A[2 * ei,2*jj] -= rpi
            # and b[2*ei] += 0
            # Eqn 2
            A[2 * ei + 1, 2*ii + 1] += rpi
            A[2 * ei + 1, 2*jj + 1] -= rpi
            # and b[2*ei + 1] += 0

        # Add penalty for boundary "constraints"
        bstart = 2 * numEdges
        if type(ai) == type(None):
            print('Boundary constraints %d %d' % bdyVertConstraints.shape)
        for bi in range(numBdyVerts):
            bvi = int(bdyVertConstraints[bi,0])
            bvth = bdyVertConstraints[bi, 1]
            if type(ai) == type(None):
                print('  %2d  %2d  th %g' % (bi, bvi, bvth*180/np.pi))
            A[2 * bi + bstart, 2 * bvi] += self.Lambda
            b[2 * bi + bstart] += self.Lambda * np.cos(4*bvth)
            A[2 * bi + 1 + bstart, 2 * bvi + 1] += self.Lambda
            b[2 * bi + 1 + bstart] += self.Lambda * np.sin(4*bvth)

        sstart = bstart + 2 * numBdyVerts
        if numSmooth > 0 and numSmooth != numVerts:
            print('error: expected verts (%d) and smooth (%d) of same size' % (numVerts, numSmooth))
        for si in range(numSmooth):
            A[si + sstart, 2 * si] += self.Lambda * ai[si,0]
            A[si + sstart, 2 * si + 1] += self.Lambda * ai[si,1]
            b[si + sstart] += self.Lambda

        return A, b

    def SolveMatrix(self, A, b):
        numVerts = int(A.shape[1] / 2) # np.max(edges) - np.min(edges) + 1
        M = np.matrix(A.transpose())*np.matrix(A)
        AtB = np.matrix(A.transpose()) * np.matrix(b).transpose()
        X = np.linalg.solve(M, AtB)
        # Reshape so rows are coordinates:
        Xc = np.reshape(X, (numVerts, 2))
        # Renormalize coordinates onto unit circle:
        Xn = np.linalg.norm(Xc, axis=1)
        Xn2 = np.reshape(Xn, (numVerts,1))
        ai = Xc / Xn2
        return ai

    def ComputeFrameField(self, pdIn, bdyVertConstraints, edges):
        numVerts = pdIn.GetNumberOfPoints()
        A, b = self.ComputeMatrix(numVerts, edges, bdyVertConstraints)
        ai = self.SolveMatrix(A, b)
        for si in range(self.SmoothingIterations):
            A, b = self.ComputeMatrix(numVerts, edges, bdyVertConstraints, ai)
            ai = self.SolveMatrix(A, b)
        return ai

    def FrameFieldVectors(self, ai):
        from vtkmodules.util.numpy_support import numpy_to_vtk as ntov

        nc = int(ai.shape[0])
        v0 = ntov(np.hstack((ai,np.zeros((nc,1)))))
        v0.SetName('FrameFieldX')
        v1 = ntov(np.cross(np.hstack((ai,np.zeros((nc,1)))), np.array([0,0,1])))
        v1.SetName('FrameFieldY')
        return v0, v1

@smproxy.filter(label='Planar Frame Field Strain')
@smhint.xml("""
    <ShowInMenu category="Frame field"/>
    <RepresentationType port="0" view="RenderView" type="Surface" />
""")
@smproperty.input(name="Triangle mesh", port_index=0)
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=False)
class PlanarFrameStrain(VTKPythonAlgorithmBase):
    """Identify the cells of a frame field's support where the
    frame field is singular by computing the "strain" induced
    when transforming frames in a circuit around the the cell
    boundary."""
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkPolyData")

    def RequestData(self, request, inInfoVec, outInfoVec):
        from vtkmodules.vtkCommonDataModel import vtkPolyData
        from vtkmodules.vtkFiltersCore import vtkTriangleFilter as TriangleFilter
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        from vtkmodules.util.numpy_support import numpy_to_vtk as ntov

        # We will just add cell data to the input:
        pdIn = vtkPolyData.GetData(inInfoVec[0], 0)
        output = vtkPolyData.GetData(outInfoVec, 0)
        tf = TriangleFilter()
        tf.SetInputDataObject(0, pdIn)
        tf.Update()
        output.ShallowCopy(tf.GetOutputDataObject(0))

        pdata = pdIn.GetPointData()
        v0 = vton(pdata.GetArray('FrameFieldX'))
        v1 = vton(pdata.GetArray('FrameFieldY'))
        conn = vton(output.GetPolys().GetConnectivityArray())
        conn = np.reshape(conn, (int(conn.shape[0]/3), 3))
        strain = self.FindStrain(v0, v1, conn)
        sdata = ntov(strain)
        sdata.SetName('strain')
        output.GetCellData().AddArray(sdata)
        return 1

    def FindStrain(self, v0, v1, conn):
        """Compute strain of a frame field, which can be used to
        identify singularities.

        The definition of singular from Ray & Sokolov is:
        A tet is called singular if any of its triangles is singular.
        A triangle ijk is singular if and only if Rij × Rjk × Rki = Id,
        where Rij denotes the rotation matrix that brings the frame fi
        to the frame fj.

        This filter computes the sum of the angles around each triangle
        as the strain. Thresholding cells with non-zero strain will
        identify cells in the frame field's support where the field is
        singular. Think of the computation as an angle defect.
        """
        strain = np.zeros(conn.shape[0])
        ii = 0
        for tri in conn:
            i0 = tri[0]
            i1 = tri[1]
            i2 = tri[2]
            # minimal rotation to bring frame 0 into alignment with frame 1.
            # angle will always be in [-90, 90] degrees
            a01 = self.MinimalRotation(v0[i0,:], v0[i1,:])
            a12 = self.MinimalRotation(v0[i1,:], v0[i2,:])
            a20 = self.MinimalRotation(v0[i2,:], v0[i0,:])
            strain[ii] = a01 + a12 + a20
            if ii == 80:
                print('Cell 80 ids: %d %d %d' % (i0, i1, i2))
                print('Cell 80 vecs: %s %s %s' % (str(v0[i0,:]), str(v0[i1,:]), str(v0[i2,:])))
                print('Cell 80 angles: %g %g %g -> %g (%g %g %g)' % (a01, a12, a20, strain[ii], a01 * 180/np.pi, a12*180/np.pi, a20*180/np.pi))
            ii += 1
        return strain

    def MinimalRotation(self, v0, v1):
        angle = np.arcsin(np.cross(v0, v1)[2])
        if angle > np.pi/4:
            angle = angle - np.pi/2
        elif angle < -np.pi/4:
            angle = -np.pi/2 - angle
        return angle
