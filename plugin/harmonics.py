"""Compute real spherical harmonics of degree 4."""
import numpy as np

def Y4m4(x, y, z):
    from numpy import sqrt, pi
    x2 = x*x
    y2 = y*y
    r2 = x2 + y2 + z*z
    result = 0.75 * sqrt(35/pi) * x * y * (x2 + y2) / r2 / r2
    return result

def Y4m3(x, y, z):
    from numpy import sqrt, pi
    x2 = x*x
    y2 = y*y
    r2 = x2 + y2 + z*z
    result = 0.75 * sqrt(35/2.0/pi) * (3 * x2 - y2) * y * z / r2 / r2
    return result

def Y4m2(x, y, z):
    from numpy import sqrt, pi
    z2 = z*z
    r2 = x*x + y*y + z2
    result = 0.75 * sqrt(5/pi) * x * y * (7 * z2 - r2) / r2 / r2
    return result

def Y4m1(x, y, z):
    from numpy import sqrt, pi
    z2 = z*z
    r2 = x*x + y*y + z2
    result = 0.75 * sqrt(5/2.0/pi) * y * z * (7 * z2 - 3 * r2) / r2 / r2
    return result

def Y4z0(x, y, z):
    from numpy import sqrt, pi
    z2 = z*z
    r2 = x*x + y*y + z2
    result = 3/16.0 * sqrt(1/pi) * (35 * z2 * z2 - 30 * z2 * r2 + 3 * r2 * r2) / r2 / r2
    return result

def Y4p1(x, y, z):
    from numpy import sqrt, pi
    z2 = z*z
    r2 = x*x + y*y + z2
    result = 3/4.0 * sqrt(5/2.0/pi) * x * z * (7 * z2 - 3 * r2) / r2 / r2
    return result

def Y4p2(x, y, z):
    from numpy import sqrt, pi
    x2 = x*x
    y2 = y*y
    z2 = z*z
    r2 = x2 + y2 + z2
    result = 3/8.0 * sqrt(5/pi) * (x2 - y2) * (7 * z2 - r2) / r2 / r2
    return result

def Y4p3(x, y, z):
    from numpy import sqrt, pi
    x2 = x*x
    y2 = y*y
    r2 = x2 + y2 + z*z
    result = 3/4.0 * sqrt(35/2.0/pi) * (x2 - 3 * y2) * x * z / r2 / r2
    return result

def Y4p4(x, y, z):
    from numpy import sqrt, pi
    x2 = x*x
    y2 = y*y
    r2 = x2 + y2 + z*z
    result = 3/16.0 * sqrt(35/pi) * (x2 * (x2 - 3 * y2) - y2 * (3 * x2 - y2)) / r2 / r2
    return result

def Y(x, y, z):
    """Given arrays x, y, and z, evaluate all 4-th order spherical
    harmonic basis functions and return a matrix whose columns are
    Y4m4, Y4m3, ..., Y4p3, Y4p4.
    """
    from numpy import sqrt, pi
    x2 = x*x
    y2 = y*y
    z2 = z*z
    r2 = x2 + y2 + z2
    Yf = np.vstack((
        (3/ 4.0 * sqrt(35/pi) * x * y * (x2 - y2) / r2 / r2),
        (3/ 4.0 * sqrt(35/2.0/pi) * (3 * x2 - y2) * y * z / r2 / r2),
        (3/ 4.0 * sqrt(5/pi) * x * y * (7 * z2 - r2) / r2 / r2),
        (3/ 4.0 * sqrt(5/2.0/pi) * y * z * (7 * z2 - 3 * r2) / r2 / r2),
        (3/16.0 * sqrt(1/pi) * (35 * z2 * z2 - 30 * z2 * r2 + 3 * r2 * r2) / r2 / r2),
        (3/ 4.0 * sqrt(5/2.0/pi) * x * z * (7 * z2 - 3 * r2) / r2 / r2),
        (3/ 8.0 * sqrt(5/pi) * (x2 - y2) * (7 * z2 - r2) / r2 / r2),
        (3/ 4.0 * sqrt(35/2.0/pi) * (x2 - 3 * y2) * x * z / r2 / r2),
        (3/16.0 * sqrt(35/pi) * (x2 * (x2 - 3 * y2) - y2 * (3 * x2 - y2)) / r2 / r2)
        )).transpose()
    return Yf

def Yi(x, y, z):
    """Given complex-valued arrays x, y, and z, evaluate all 4-th order spherical
    harmonic basis functions and return a matrix whose columns are
    Y4m4, Y4m3, ..., Y4p3, Y4p4.
    """
    from numpy import sqrt, pi
    Yf = np.vstack((
        3/16.*sqrt(35/2/pi)*np.power(x - 1j * y, 4),
        3/8. *sqrt(35/pi)*np.power(x - 1j * y, 3) * z,
        3/8. *sqrt(5/2/pi)*np.power(x - 1j * y, 2) * (7*z*z - 1),
        3/8. *sqrt(5/pi)*(x - 1j * y) * z * (7 * z*z - 3),
        3/16.*sqrt(1/pi)*(35*z*z*z*z - 30*z*z + 3),
        -3/8.*sqrt(5/pi)*((x + 1j * y) * z * (7 * z * z - 3)),
        3/8. *sqrt(5/2/pi)*np.power(x + 1j * y, 2) * (7 * z * z - 1),
        -3/8.*sqrt(35/pi)*np.power(x + 1j * y, 3) * z,
        3/16.*sqrt(35/2/pi)*np.power(x + 1j * y, 4)
        )).transpose()
    return Yf

# def Ytp(x, y, z):
#     """Given arrays x, y, and z, compute theta and phi (assuming r = 1), then
#     evaluate all 4-th order spherical harmonic basis functions and
#     return a matrix whose columns are
#     Y4m4, Y4m3, ..., Y4p3, Y4p4.
#     """
#     from numpy import sqrt, pi, exp
#     th = np.arccos(z)
#     cth = z
#     sth = np.sin(th)
#     s2th = sth * sth
#     c2th = cth * cth
#     ph = np.arcsin(y / sth)
#     cph = z / sth
#     sph = y / sth
#     Yf = np.vstack((
#         3/16. * sqrt(35/2/pi) * exp(-4j*ph)
#         )).transpose()
#     return Yf
