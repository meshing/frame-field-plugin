"""This module provides ParaView filters for extracting point normals
of interest from tetrahedral meshes for use in constraining frame fields.
It is based on work by N. Ray and D. Sokolov. These are implemented using
VTKPythonAlgorithmBase and make extensive use of NumPy."""

from paraview.util.vtkAlgorithm import *
import numpy as np
import scipy as sp
import os
import sys
import inspect
sys.path.insert(0, os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
from harmonics import Y, Yi
from rotation import *

def clampUnitDelta(x):
    return -1 if x < -1 else (1 if x > 1 else x)

@smproxy.filter(label='Evaluate Harmonics')
@smhint.xml("""
    <ShowInMenu category="Frame field"/>
    <RepresentationType port="0" view="RenderView" type="Surface" />
""")
@smproperty.input(name="Surface mesh", port_index=0)
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=False)
class EvalHarmonics(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkPolyData")
        self.Coefficients = np.zeros((9,1))
        self.Alpha = 0.0
        self.Beta = 0.0
        self.Gamma = 0.0

    @smproperty.doublevector(name="Coefficients", number_of_elements=9, default_values=[0,0,0,  0,0,0,  0,0,0])
    def SetCoefficients(self, coeff, c1=None, c2=None, c3=None, c4=None, c5=None, c6=None, c7=None, c8=None):
        if type(coeff) != type(0.0):
            if coeff != self.Coefficients:
                self.Coefficients = np.reshape(np.array(coeff), (9, 1))
                self.Modified()
        else:
            self.Coefficients = np.reshape(np.array([coeff, c1, c2, c3, c4, c5, c6, c7, c8]), (9, 1))
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetCoefficients(self):
        return self.Coefficients

    @smproperty.doublevector(name="Alpha", default_values=[0])
    @smdomain.doublerange(min=-180, max=180)
    def SetAlpha(self, angle):
        if angle != self.Alpha:
            self.Alpha = angle
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetAlpha(self):
        return self.Alpha

    @smproperty.doublevector(name="Beta", default_values=[0])
    @smdomain.doublerange(min=-180, max=180)
    def SetBeta(self, angle):
        if angle != self.Beta:
            self.Beta = angle
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetBeta(self):
        return self.Beta

    @smproperty.doublevector(name="Gamma", default_values=[0])
    @smdomain.doublerange(min=-180, max=180)
    def SetGamma(self, angle):
        if angle != self.Gamma:
            self.Gamma = angle
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetGamma(self):
        return self.Gamma

    def RequestData(self, request, inInfoVec, outInfoVec):
        from vtkmodules.vtkCommonDataModel import vtkPolyData
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        from vtkmodules.util.numpy_support import numpy_to_vtk as ntov
        pdIn = vtkPolyData.GetData(inInfoVec[0], 0)
        output = vtkPolyData.GetData(outInfoVec, 0)
        output.ShallowCopy(pdIn)

        if np.any(self.Coefficients != 0.0):
            coeff = self.Coefficients
        else:
            # Initialize our degree-4 coefficients to the polynomial
            # x^4 + y^4 + z^4 + k = 0:
            coeff = atilde()
            # if np.any(self.Coefficients != 0.0):
            #     coeff = self.Coefficients
            # else:
            #     coeff = np.array([1,0,0, 0,0,0, 0,0,0])

            # Rotate the polynomial via Euler angles to obtain a new basis:
            d2r = np.pi/180.0
            # coeff = RbEulerZYZ(self.Alpha * d2r, self.Beta * d2r, self.Gamma * d2r) * coeff
            coeff = RbEulerXYZ(self.Alpha * d2r, self.Beta * d2r, self.Gamma * d2r) * coeff

        # Now evaluate the polynomial at all points on the input polydata
        # (which are assumed to lie on the unit-radius sphere).
        pcoords = vton(output.GetPoints().GetData())
        px = pcoords[:,0]
        py = pcoords[:,1]
        pz = pcoords[:,2]
        hh = Y(px, py, pz)
        val = hh * np.reshape(np.matrix(coeff), (9,1))
        arr = ntov(np.real(val))
        arr.SetName('Y')
        output.GetPointData().SetScalars(arr)
        # Save the 9-tuple of harmonic coefficients on the output
        harmonic = ntov(coeff)
        harmonic.SetName('harmonic')
        output.GetFieldData().AddArray(harmonic)
        # # For giggles, let's output the 9 basis functions as well.
        # for sfi in range(9):
        #     sf = ntov(np.real(hh[:,sfi]))
        #     sf.SetName('Y_4_%s%d' % ('m' if sfi - 4 < 0 else 'p', np.abs(sfi - 4)))
        #     output.GetPointData().AddArray(sf)
        return 1

@smproxy.filter(label='Invert Harmonic Rotation')
@smhint.xml("""
    <ShowInMenu category="Frame field"/>
    <RepresentationType port="0" view="RenderView" type="Surface" />
""")
@smproperty.input(name="Glyph", port_index=1)
@smproperty.input(name="Frame Field", port_index=0)
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=False)
class InvertHarmonicRotation(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=2, nOutputPorts=1, outputType="vtkPolyData")

    def RequestData(self, request, inInfoVec, outInfoVec):
        from vtkmodules.vtkCommonDataModel import vtkPolyData
        from vtkmodules.vtkCommonTransforms import vtkTransform
        from vtkmodules.vtkFiltersGeneral import vtkTransformFilter
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        from vtkmodules.util.numpy_support import numpy_to_vtk as ntov
        pdIn = vtkPolyData.GetData(inInfoVec[0], 0)
        glIn = vtkPolyData.GetData(inInfoVec[1], 0)
        output = vtkPolyData.GetData(outInfoVec, 0)

        coeff = pdIn.GetFieldData().GetArray('harmonic')
        if coeff == None:
            print('Input 0 must have a field-data array named "harmonic"')
            return 0
        coeff = vton(coeff)
        rr, b = closestFrame(coeff)
        # print('coeff ', coeff, ' rr ', rr)
        tfm = vtkTransform()
        tfm.SetMatrix([
            rr[0,0], rr[0, 1], rr[0,2], 0.0,
            rr[1,0], rr[1, 1], rr[1,2], 0.0,
            rr[2,0], rr[2, 1], rr[2,2], 0.0,
                0.0,      0.0,     0.0, 1.0 ])
        tff = vtkTransformFilter()
        tff.SetInputDataObject(0, glIn)
        tff.SetTransform(tfm)
        tff.Update()
        tgl = tff.GetOutput()
        output.ShallowCopy(tgl)

        return 1

@smproxy.filter(label='Frame Field')
@smhint.xml("""
    <ShowInMenu category="Frame field"/>
    <RepresentationType port="0" view="RenderView" type="Surface" />
""")
@smproperty.input(name="Surface mesh", port_index=0)
@smdomain.datatype(dataTypes=["vtkUnstructuredGrid"], composite_data_supported=False)
class FrameField(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkUnstructuredGrid")
        self.FeatureAngle = 30
        self.Iterations = 10
        self.Penalty = 100

    @smproperty.doublevector(name="FeatureAngle", default_values=[30])
    @smdomain.doublerange(min=0, max=180)
    def SetFeatureAngle(self, angle):
        if angle != self.FeatureAngle:
            self.FeatureAngle = angle
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetFeatureAngle(self):
        return self.FeatureAngle

    @smproperty.intvector(name="Iterations", default_values=[10])
    @smdomain.intrange(min=1, max=1000)
    def SetIterations(self, iterationCount):
        """Set the number of iterations to run the frame field algorithm.

        Iterations beyond the first will use prior output to constrain
        interior nodes (to allowing smoother results).
        """
        iterationCount = int(iterationCount)
        if iterationCount != self.Iterations:
            self.Iterations = iterationCount
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetIterations(self):
        return self.Iterations

    @smproperty.doublevector(name="Penalty", default_values=[100])
    @smdomain.doublerange(min=1, max=10000)
    def SetPenalty(self, penalty):
        """Set the number of iterations to run the frame field algorithm.

        Penalty beyond the first will use prior output to constrain
        interior nodes (to allowing smoother results).
        """
        if penalty != self.Penalty:
            self.Penalty = penalty
            self.Modified()

    # TODO: Any way to make this an information_only property?
    def GetPenalty(self):
        return self.Penalty

    def RequestData(self, request, inInfoVec, outInfoVec):
        from vtkmodules.vtkCommonCore import vtkPoints
        from vtkmodules.vtkCommonDataModel import vtkPolyData, vtkUnstructuredGrid
        from vtkmodules.vtkFiltersGeometry import vtkGeometryFilter as GeometryFilter
        from vtkmodules.vtkFiltersParallelDIY2 import vtkGenerateGlobalIds as GenerateGlobalIds
        from vtkmodules.vtkFiltersParallelDIY2 import vtkPointFeatures as PointFeatures
        from vtkmodules.vtkFiltersExtraction import vtkExtractEdges as ExtractEdges
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        from vtkmodules.util.numpy_support import numpy_to_vtk as ntov
        from vtkmodules.vtkIOLegacy import vtkPolyDataWriter
        tetMesh = vtkUnstructuredGrid.GetData(inInfoVec[0], 0)
        # framefield = vtkPolyData.GetData(outInfoVec, 0)
        framefield = vtkUnstructuredGrid.GetData(outInfoVec, 0)

        gi = GenerateGlobalIds()
        gi.SetInputDataObject(0, tetMesh)
        gi.Update()
        lblMesh = gi.GetOutputDataObject(0)

        pf = PointFeatures()
        pf.SetFeatureAngle(self.FeatureAngle)
        pf.SetInputConnection(gi.GetOutputPort())
        pf.Update()
        wri = vtkPolyDataWriter()
        wri.SetInputConnection(pf.GetOutputPort())
        wri.SetFileName('/tmp/pfeat.vtk')
        wri.Write()

        ee = ExtractEdges()
        ee.SetInputConnection(gi.GetOutputPort())
        ee.Update()
        edgeData = ee.GetOutput()
        if edgeData.GetNumberOfCells() == 0:
            es = GeometryFilter()
            es.SetInputConnection(gi.GetOutputPort())
            es.Update()
            edgeData = es.GetOutput()

        ai, pointMap = self.ComputeFrameField(lblMesh, constraints=pf.GetOutput(), edges=edgeData)
        v0, v1, v2 = self.FrameFieldToVectors(ai, pointMap)

        # Copy input to output, then add frame field vectors as point-data.
        framefield.ShallowCopy(lblMesh)
        # framepts = vtkPoints()
        # framepts.ShallowCopy(lblMesh.GetPoints())
        # framefield.SetPoints(framepts)
        framefield.GetPointData().AddArray(v0)
        framefield.GetPointData().AddArray(v1)
        framefield.GetPointData().AddArray(v2)
        return 1

    def AddSmoothingTerms(self, AA, bb, currentRow, edgeMap, tcoords):
        """Each edge in the mesh contributes to the energy associated with
        frame fields at its endpoints by preferring orientations in its
        direction.

        This results in frame fields that transition smoothly when otherwise
        unconstrained.
        """
        row = currentRow
        for edge in edgeMap:
            ii = edge[0]
            jj = edge[1]
            # print('  Remapped edge %d -- %d' % (ii, jj))
            for d in range(9):
                AA[row, 9 * ii + d] = 1
                AA[row, 9 * jj + d] = -1
                row += 1
        return row

    def AddNormalConstraint(self, AA, bb, nvnlne, currentRow, gid, normals):
        row = currentRow
        if len(normals) == 0:
            return row
        elif len(normals) > 1:
            # FIXME TODO: Do not just take the first normal as Z. Choose
            # the pair of normals closest to 90 degrees apart and adjust
            # to get an orthogonal pair.
            zz = normals[0]
            yy = np.cross(zz, normals[1])
            xx = np.cross(zz, yy)
        else:
            zz = normals[0]
            xx = np.array([1, 0, 0]) if zz[0] < 0.7 else np.array([0, 1, 0])
            yy = np.cross(zz, xx)
            xx = np.cross(zz, yy)
        # print('    ', xx, yy, zz, gid)
        # Now find Euler angles.
        # Consider even more axis orderings far from singularity.
        rb = None
        if np.abs(zz[0]) < np.pi / 4:
            bt = np.arcsin(clampUnitDelta(zz[0]))
            gm = np.arccos(clampUnitDelta(xx[0] / np.cos(bt)))
            al = np.arccos(clampUnitDelta(zz[2] / np.cos(bt)))
            rb = Rbx(al) * Rby(bt) * Rbz(gm)
            # print('    Euler XYZ ', gid, al, bt, gm, '  Rb*a~ ', rb*atilde())
        elif np.abs(zz[2]) < np.pi / 4:
            bt = np.arcsin(clampUnitDelta(xx[2]))
            al = np.arcsin(clampUnitDelta(yy[2] / np.cos(bt)))
            gm = -np.arccos(clampUnitDelta(xx[0] / np.cos(bt)))
            rb = Rbz(gm) * Rby(bt) * Rbx(al)
            # print('    Euler ZYX ', gid, al, bt, gm, '  Rb*a~ ', rb*atilde())
        else:
            gm = np.arcsin(clampUnitDelta(xx[1]))
            al = np.arcsin(clampUnitDelta(-zz[1]/np.cos(gm)))
            bt = np.arccos(clampUnitDelta(xx[0]/np.cos(gm)))
            rb = Rby(bt) * Rbz(gm) * Rbx(al)
            # print('    Euler YZX ', gid, al, bt, gm, '  Rb*a~ ', rb*atilde())
        h0 = rb * np.matrix([[1, 0, 0,   0, 0, 0,    0, 0, 0]]).transpose()
        h4 = rb * np.matrix([[0, 0, 0,   0, 1, 0,    0, 0, 0]]).transpose()
        h8 = rb * np.matrix([[0, 0, 0,   0, 0, 0,    0, 0, 1]]).transpose()
        nv, nl, ne = nvnlne
        for dd in range(9):
            AA[row, 9 * gid + dd] = self.Penalty
            AA[row, 9 * nv + 2 * gid + 0] = self.Penalty * h0[dd]
            AA[row, 9 * nv + 2 * gid + 1] = self.Penalty * h8[dd]
            bb[row] = self.Penalty * np.sqrt(7/12) * h4[dd]
            row += 1
        return row

    def AddNormalConstraints(self, AA, bb, nvnlne, currentRow, gidMap, pointMap, constraints):
        """Iterate over vertex cells of the constraints polydata and add
        constraint equations for each point. Note that when multiple consecutive
        vertex cells refer to the same point, the normals will be accumulated
        into a single frame constraint.
        """
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        vconn = constraints.GetVerts().GetConnectivityArray()
        vnorm = vton(constraints.GetCellData().GetNormals())
        vgids = vton(constraints.GetCellData().GetGlobalIds())
        row = currentRow
        lastVertex = -1
        normals = []
        for vv in range(vconn.GetNumberOfValues()):
            vertex = vconn.GetValue(vv)
            if lastVertex != vertex and len(normals) > 0:
                # We've accumulated normals from all the vertices sharing
                # the 'lastVertex' point-ID. Process them now.
                # Compute a best-fit frame from all available normals.
                # print('point ', pointMap[gidMap[vgids[vv - 1]]], ' normals ', normals)
                row = self.AddNormalConstraint(AA, bb, nvnlne, row, pointMap[gidMap[vgids[vv - 1]]], normals)
                # Reset and start processing new vertex
                normals = [vnorm[vv],]
                lastVertex = vertex
            else:
                normals.append(vnorm[vv])
                lastVertex = vertex
        # Now handle final constraint
        if lastVertex >= 0:
            # print('point ', pointMap[gidMap[vgids[vconn.GetMaxId()]]], ' normals ', normals)
            row = self.AddNormalConstraint(AA, bb, nvnlne, row, pointMap[gidMap[vgids[vconn.GetMaxId()]]], normals)
        return row

    def AddLocalOptimizationConstraints(self, AA, bb, nvnlne, currentRow, ai):
        nv, nl, ne = nvnlne
        row = currentRow
        assert nv == ai.shape[0]
        for aa in range(ai.shape[0]):
            coeffs = np.matrix(np.reshape(ai[aa,:], (9,1)))
            cx = Ebx() * coeffs
            cy = -Eby() * coeffs # TODO: Correct?
            cz = Ebz() * coeffs
            AA[np.arange(row,row + 9), np.arange(9 * aa, 9 * aa + 9)] = self.Penalty
            AA[row:(row + 9), 9 * nv + 2 * nl + 3 * aa + 0] = -self.Penalty * cx
            AA[row:(row + 9), 9 * nv + 2 * nl + 3 * aa + 1] = -self.Penalty * cy
            AA[row:(row + 9), 9 * nv + 2 * nl + 3 * aa + 2] = -self.Penalty * cz
            bb[row:(row + 9)] = self.Penalty * coeffs
            row += 9
            # AA[row
            # URHERE: Use ranges instead
            # for dd in range(9):
            #     AA[row, 9 * aa + dd] = self.Penalty
            #     AA[row, 9 * nv + 2 * nl + 3 * aa + 0] = -self.Penalty * cx[dd]
            #     AA[row, 9 * nv + 2 * nl + 3 * aa + 1] = -self.Penalty * cy[dd]
            #     AA[row, 9 * nv + 2 * nl + 3 * aa + 2] = -self.Penalty * cz[dd]
            #     bb[row] = self.Penalty * coeffs[dd]
            #     row += 1
        return row

    def ExtractCoefficients(self, xx, nvnlne):
        """Extract the subset of the frame field solution related to orientation."""
        nv, nl, ne = nvnlne
        ai = np.reshape(np.array(xx[0:(9*nv)]), (nv, 9))
        return ai

    def ComputeFrameField(self, tetMesh, constraints, edges):
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        from scipy.sparse import lil_matrix
        from scipy.sparse.linalg import lsqr
        print('Compute')

        # I. Use global IDs to create maps for constraints and mesh edges.
        #    The result is a relabeling of vertices so that all constrained
        #    vertices come first, followed by unconstrained ones.
        #    For now, all constrained vertices are from the boundary, but
        #    that need not be the case, especially with user-edited frame fields.
        ccoords = vton(constraints.GetPoints().GetData())
        cgids = vton(constraints.GetCellData().GetGlobalIds())
        tcoords = vton(tetMesh.GetPoints().GetData())
        tgids = vton(tetMesh.GetPointData().GetGlobalIds())
        cid = 0
        cset = set()
        for cgid in cgids:
            cset.add(cgid)
        pid = 0
        off = len(cset)
        pointMap = {}
        gidMap = {}
        for ii in range(tgids.shape[0]):
            if tgids[ii] in cset:
                pointMap[ii] = pid
                pid += 1
            else:
                pointMap[ii] = off
                off += 1
            gidMap[tgids[ii]] = ii
        print('  pointMap size ', len(pointMap), ' constrained nodes ', len(cset), ' num constraints ', len(cgids))
        # print(np.sort([x for x in gidMap.keys()]))

        # II. Translate edge connectivity into new vertex ordering.
        econn = vton(edges.GetLines().GetConnectivityArray())
        egids = vton(edges.GetPointData().GetGlobalIds())
        # TODO: The following assumes no lines are polylines!
        #       We could inspect edges.GetLines().GetOffsetsArray()... but then
        #       we would need to handle polylines. Bleh.
        numberOfEdges = int(econn.shape[0] / 2)
        econn = np.reshape(econn, (numberOfEdges, 2))
        edgeData = np.zeros((numberOfEdges, 2))
        # print(np.sort(egids))
        # print(np.sort(tgids))
        for eidx in range(numberOfEdges):
            # print('econn ', econn[eidx,:], ' egids ', egids[econn[eidx,0]], egids[econn[eidx,1]] )
            edgeData[eidx,0] = pointMap[gidMap[egids[econn[eidx,0]]]]
            edgeData[eidx,1] = pointMap[gidMap[egids[econn[eidx,1]]]]

        # III. Iteratively create/update the frame field
        nv = len(pointMap)
        nl = len(cset)
        ne = edgeData.shape[0]
        ncols = 9 * nv + 2 * nl + 3 * nv
        ai = np.zeros((tetMesh.GetNumberOfPoints(), 9))
        for smoothingIteration in range(self.Iterations):
            print('Iteration %d' % smoothingIteration)
            nrows = 9 * (ne + nl) if smoothingIteration == 0 else 9 * (ne + nl + nv)
            AA = lil_matrix((nrows, ncols))
            bb = np.zeros((nrows, 1))
            currentRow = 0
            currentRow = self.AddSmoothingTerms(AA, bb, currentRow, edgeData, tcoords)
            print('  Smoothing took %d rows' % currentRow)
            currentRow = self.AddNormalConstraints(AA, bb, (nv, nl, ne), currentRow, gidMap, pointMap, constraints)
            print('  Smoothing+Normals took %d rows' % currentRow)
            if smoothingIteration > 0:
                currentRow = self.AddLocalOptimizationConstraints(AA, bb, (nv, nl, ne), currentRow, ai)
            print('  Smoothing+Normals+LocalOpt took %d rows of %d' % (currentRow, AA.shape[0]))
            # print('AA %s bb %s nv %d nl %d ne %d' % (str(AA.shape), str(bb.shape), nv, nl, ne))
            # with np.printoptions(precision=1, suppress=True, linewidth=132, threshold=10000):
            #     print(AA.toarray())
            #     print(bb)
            xx, status, itCount, *_ = lsqr(AA, bb, show=False)
            print('  Complete: reason %d count %d %s' % (status, itCount, str(xx.shape)))
            ai = self.ExtractCoefficients(xx, (nv, nl, ne))
        # Compute the inverse of the point map so the ai can be mapped back to mesh vertices
        invPointMap = {v: k for k, v in pointMap.items()}
        return (ai, invPointMap)

    def FrameFieldToVectors(self, ai, pointMap):
        from vtkmodules.util.numpy_support import numpy_to_vtk as ntov
        print('FramesToVectors')
        v0 = np.zeros((ai.shape[0], 3))
        v1 = np.zeros((ai.shape[0], 3))
        v2 = np.zeros((ai.shape[0], 3))
        for aa in range(ai.shape[0]):
            q = ai[aa,:]
            f, _ = closestFrame(q)
            # print(aa, pointMap[aa], q)
            v0[pointMap[aa],:] = f[:,0].transpose()
            v1[pointMap[aa],:] = f[:,1].transpose()
            v2[pointMap[aa],:] = f[:,2].transpose()
        vv0 = ntov(v0)
        vv1 = ntov(v1)
        vv2 = ntov(v2)
        vv0.SetName('FrameX')
        vv1.SetName('FrameY')
        vv2.SetName('FrameZ')
        return vv0, vv1, vv2

@smproxy.filter(label='Mark Singularities')
@smhint.xml("""
    <ShowInMenu category="Frame field"/>
    <RepresentationType port="0" view="RenderView" type="Surface" />
""")
@smproperty.input(name="mesh", port_index=0)
@smdomain.datatype(dataTypes=["vtkUnstructuredGrid"], composite_data_supported=False)
class MarkSingularities(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkUnstructuredGrid")

    def FrameFromVectors(self, ptid, v0, v1, v2):
        return np.matrix(np.vstack((v0[ptid,:], v1[ptid,:], v2[ptid,:])).transpose())

    def RequestData(self, request, inInfoVec, outInfoVec):
        from vtkmodules.vtkCommonCore import vtkPoints, vtkUnsignedCharArray
        from vtkmodules.vtkCommonDataModel import vtkPolyData, vtkUnstructuredGrid, vtkTetra, VTK_TETRA
        from vtkmodules.util.numpy_support import vtk_to_numpy as vton
        tetMesh = vtkUnstructuredGrid.GetData(inInfoVec[0], 0)
        output = vtkUnstructuredGrid.GetData(outInfoVec, 0)
        output.ShallowCopy(tetMesh)

        pd = tetMesh.GetPointData()
        v0 = vton(pd.GetArray('FrameX'))
        v1 = vton(pd.GetArray('FrameY'))
        v2 = vton(pd.GetArray('FrameZ'))
        sing = vtkUnsignedCharArray()
        sing.SetName('singular')
        it = tetMesh.NewCellIterator()
        it.InitTraversal()
        while not it.IsDoneWithTraversal():
            if it.GetCellType() != VTK_TETRA:
                sing.InsertNextValue(2)
                continue
            # Cell is a tetrahedron. Test each face for singularity.
            ids = it.GetPointIds()
            ids = [ids.GetId(ii) for ii in range(ids.GetNumberOfIds())]
            frames = [self.FrameFromVectors(ptid, v0, v1, v2) for ptid in ids]
            stet = False
            for face in range(4):
                fconn = vtkTetra.GetFaceArray(face)
                stet = stet or isSingular((frames[fconn[0]], frames[fconn[1]], frames[fconn[2]]))
            sing.InsertNextValue(1 if stet else 0)
            it.GoToNextCell()
        output.GetCellData().AddArray(sing)
        return 1
