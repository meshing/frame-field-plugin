import numpy as np

np.set_printoptions(precision=3)
np.set_printoptions(suppress=True)

def isclose(x, y, rtol=1.e-5, atol=1.e-8):
    return abs(x-y) <= atol + rtol * abs(y)

def euler_angles_from_rotation_matrix(R):
    '''
    From a paper by Gregory G. Slabaugh (undated),
    "Computing Euler angles from a rotation matrix
    '''
    phi = 0.0
    if isclose(R[2,0],-1.0):
        theta = np.pi/2.0
        psi = np.arctan2(R[0,1],R[0,2])
    elif isclose(R[2,0],1.0):
        theta = -np.pi/2.0
        psi = np.arctan2(-R[0,1],-R[0,2])
    else:
        theta = -np.arcsin(R[2,0])
        cos_theta = np.cos(theta)
        psi = np.arctan2(R[2,1]/cos_theta, R[2,2]/cos_theta)
        phi = np.arctan2(R[1,0]/cos_theta, R[0,0]/cos_theta)
    return psi, theta, phi

def Rbz(gamma):
    """Construct a rotation matrix about the z axis for degree-4 spherical harmonic shape functions.

    The gamma parameter is the angle (in radians) about which to rotate around the z axis.
    """
    cg = np.cos(gamma * np.array([1, 2, 3, 4]))
    sg = np.sin(gamma * np.array([1, 2, 3, 4]))
    rbz = np.matrix([
        [ cg[3], 0, 0, 0, 0, 0, 0, 0, sg[3]],
        [ 0, cg[2], 0, 0, 0, 0, 0, sg[2], 0],
        [ 0, 0, cg[1], 0, 0, 0, sg[1], 0, 0],
        [ 0, 0, 0, cg[0], 0, sg[0], 0, 0, 0],
        [ 0, 0, 0, 0,     1,     0, 0, 0, 0],
        [ 0, 0, 0,-sg[0], 0, cg[0], 0, 0, 0],
        [ 0, 0,-sg[1], 0, 0, 0, cg[1], 0, 0],
        [ 0,-sg[2], 0, 0, 0, 0, 0, cg[2], 0],
        [-sg[3], 0, 0, 0, 0, 0, 0, 0, cg[3]]
        ])
    return rbz

def Rbxpi2():
    """Construct a rotation matrix about the x axis for degree-4 spherical harmonic shape functions.

    The rotation is a fixed pi/2 radians (90 degrees).
    """
    # Denom 4
    s14 = np.sqrt(14)/4.0
    s24 = np.sqrt(2)/4.0
    s54 = np.sqrt(5)/4.0
    s74 = np.sqrt(7)/4.0
    # Denom 8
    s35 = np.sqrt(35)/8.0
    s58 = np.sqrt(5)/8.0
    # Rational
    q34 = 0.75
    q38 = 0.375
    q12 = 0.5
    q18 = 0.125
    rbx = np.matrix([
        [  0.0,  0.0,  0.0,  0.0,  0.0,  s14,  0.0, -s24,  0.0 ],
        [  0.0, -q34,  0.0,  s74,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  0.0,  s24,  0.0,  s14,  0.0 ],
        [  0.0,  s74,  0.0,  q34,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  q38,  0.0,  s54,  0.0,  s35 ],
        [ -s14,  0.0, -s24,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  s54,  0.0,  q12,  0.0, -s74 ],
        [  s24,  0.0, -s14,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  s35,  0.0, -s74,  0.0,  q18 ],
        ])
    return rbx

def Rby(beta):
    rbx = Rbxpi2()
    rbz = Rbz(beta)
    rby = rbx.transpose() * rbz * rbx
    return rby

def Rbx(alpha):
    rby = Rby(np.pi/2)
    rbz = Rbz(alpha)
    rbx = rby * rbz * rby.transpose()
    return rbx

def RbEulerZYZ(alpha, beta, gamma):
    """Return a 9x9 degree-4 radial basis rotation matrix
    equivalent to a frame field rotation specified as Z-Y-Z Euler angles.
    """
    rbe = Rbz(gamma)*Rbxpi2().transpose()*Rbz(beta)*Rbxpi2()*Rbz(alpha)
    return rbe

def RbEulerXYZ(alpha, beta, gamma):
    """Return a 9x9 degree-4 radial basis rotation matrix
    equivalent to a frame field rotation specified as X-Y-Z Euler angles.

    Note that this method is more computationally expensive than RbEulerZYZ
    as it involves more matrix multiplications.
    """
    rbe = Rbx(alpha)*Rby(beta)*Rbz(gamma)
    return rbe

def Ebx():
    s0 = np.sqrt(10)
    s2 = np.sqrt(2)
    s3 = 3/np.sqrt(2)
    s7 = np.sqrt(7/2.)
    ebx = np.matrix([
        [  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  -s2,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  -s7,  0.0,  -s2 ],
        [  0.0,  0.0,  0.0,  0.0,  0.0,  -s3,  0.0,  -s7,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  -s0,  0.0,  -s3,  0.0,  0.0 ],
        [  0.0,  0.0,  0.0,   s0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,  0.0,   s3,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,   s7,  0.0,   s3,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [   s2,  0.0,   s7,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,   s2,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        ])
    return ebx

def Eby():
    s0 = np.sqrt(10)
    s2 = np.sqrt(2)
    s3 = 3/np.sqrt(2)
    s7 = np.sqrt(7/2.)
    eby = np.matrix([
        [  0.0,   s2,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  -s2,  0.0,   s7,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,  -s7,  0.0,   s3,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,  0.0,  -s3,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  0.0,  -s0,  0.0,  0.0,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,   s0,  0.0,  -s3,  0.0,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  0.0,   s3,  0.0,  -s7,  0.0 ],
        [  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,   s7,  0.0,  -s2 ],
        [  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,   s2,  0.0 ],
        ]).transpose()
    return eby

def Ebz():
    ebz = np.matrix([
        [  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  4. ],
        [  0.,  0.,  0.,  0.,  0.,  0.,  0.,  3.,  0. ],
        [  0.,  0.,  0.,  0.,  0.,  0.,  2.,  0.,  0. ],
        [  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0. ],
        [  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0. ],
        [  0.,  0.,  0., -1.,  0.,  0.,  0.,  0.,  0. ],
        [  0.,  0., -2.,  0.,  0.,  0.,  0.,  0.,  0. ],
        [  0., -3.,  0.,  0.,  0.,  0.,  0.,  0.,  0. ],
        [ -4.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0. ],
        ])
    return ebz

def atilde():
    return np.matrix([0, 0, 0, 0, np.sqrt(7/12.), 0, 0, 0, np.sqrt(5/12.)]).transpose()

def Rx(theta):
    st = np.sin(theta)
    ct = np.cos(theta)
    return np.matrix([[1, 0, 0], [0, ct, -st], [0, st, ct]])

def Ry(theta):
    st = np.sin(theta)
    ct = np.cos(theta)
    return np.matrix([[ct, 0, st], [0, 1, 0], [-st, 0, ct]])

def Rz(theta):
    st = np.sin(theta)
    ct = np.cos(theta)
    return np.matrix([[ct, -st, 0], [st, ct, 0], [0, 0, 1]])

def closestFrame(sph):
    f = np.matrix(np.eye(3))
    a = atilde()
    s = 0.1
    eps = 1e-4
    q = sph / np.linalg.norm(sph)
    ni = 0
    while True:
        g = np.array([
            float(q.transpose() * Ebx() * a),
            -float(q.transpose() * Eby() * a),
            float(q.transpose() * Ebz() * a)])
        sg = s * g
        nqma = np.linalg.norm(q - a.transpose())
        # print(ni, nqma, np.sum(g * g), q - a.transpose())
        # print(g, np.sum(g*g), q, a.transpose(), nqma)
        # if nqma < eps or (ni > 2 and np.all(np.sum(g * g) < eps)):
        if np.all(np.sum(g * g) < eps):
            # print('  Done @ %d' % ni)
            break
        if ni > 150:
            break
        # Transform the spherical basis coefficients by a small rotation toward the nearest frame
        rb = Rbx(sg[0]) * Rby(sg[1]) * Rbz(sg[2])
        a = rb * a
        # Transform the frame vectors by the same amount
        rr =  Rx(sg[0]) *  Ry(sg[1]) *  Rz(sg[2])
        f = rr * f
        ni += 1
    # print(ni, 'iterations')
    if ni > 150:
        print('\nUnconverged!\n  q = %s\n  a = %s\n  ||g|| %g\n\n\n' % (str(q), str(a.transpose()), np.sum(g * g)))
    return f, a

def buildSO3classes():
    # SO(3) is the set of congruence classes of axis permutations that do not include reflection
    # (i.e., rotations that preserve handedness). All entries of SO(3) are orthonormal matrices.
    # We can determine which class a matrix M is in by taking the Frobenius norm of the difference
    # between M and one entry from each congruence class; the entry smallest Frobenius norm is
    # congruent to M.

    # I. Construct a lot (64) of rotation matrices. Some will be identical.
    mm = []
    for i in range(4):
        for j in range(4):
            for k in range(4):
                mm.append(np.round(Rx(i*np.pi/2)*Ry(j*np.pi/2)*Rz(k*np.pi/2)))
    # II. Find the unique subset of mm. These are entries in the congruence classes of SO(3)
    um = []
    # um.append(mm[0])
    for mc in mm:
        found = False
        for uu in um:
            if np.all(np.abs(uu - mc) < 1):
                found = True
                break
        if not found:
            um.append(mc)
    assert len(um) == 24, 'Expected 24 entries in SO(3).'
    return um

so3 = buildSO3classes()

def computeSO3Class(aa, bb):
    dd = [np.sum(np.array(aa - bb*vv)*np.array(aa - bb*vv)) for vv in so3]
    ii = np.argmin(dd)
    return so3[ii]

def compositeSO3Classes(seq):
    nn = len(seq)
    if nn < 2:
        return so3[0] # Identity matrix
    sclass = seq[0]
    for ii in range(1, nn):
        sclass = computeSO3Class(sclass, seq[ii % nn])
    return sclass

def isSingular(seq):
    sclass = compositeSO3Classes(seq)
    regular = np.all(np.abs(sclass - np.eye(3)) < 1e-6)
    return not regular

def test():
    I3 = np.eye(3)
    I9 = np.eye(9)
    pi2 = np.pi/2
    # Test fixed matrix:
    print('Test our fixed rotation about x of pi/2.')
    assert np.all(np.abs(Rbxpi2() * Rbx(-pi2) - I9) < 1e-8), 'Invalid x rotation matrix.'
    assert np.all(np.abs(Rbx(-pi2) * Rbxpi2() - I9) < 1e-8), 'Invalid x rotation matrix (commutative).'
    # Test that rotations of opposite sign about the same axis are inverses:
    for sign in [-1, +1]:
        for angle in [np.pi/2.0, np.pi/3.0, np.pi/4.0, np.pi/6, 0]:
            print('  Test rotations by %.1f.' % (sign * angle * 180.0 / np.pi))
            assert np.all(np.abs(Rbx(sign*angle) * Rbx(-sign*angle) - I9) < 1e-8), 'Opposing x-rotations did not cancel.'
            assert np.all(np.abs(Rby(sign*angle) * Rby(-sign*angle) - I9) < 1e-8), 'Opposing y-rotations did not cancel.'
            assert np.all(np.abs(Rbz(sign*angle) * Rbz(-sign*angle) - I9) < 1e-8), 'Opposing z-rotations did not cancel.'
    # Test non-commutative rotations are right-handed:
    print('Test non-commutative rotations are right-handed')
    # FIXME: Rby appears to be inverted!!!
    # assert np.all(np.abs(Rbxpi2() * Rby(pi2) * Rbz(pi2) - Rby(pi2)) < 1e-8), 'x(90) * y(90) * z(90) == y(90).'
    # assert np.all(np.abs(Rby(pi2) * Rbx(pi2) * Rbz(-pi2) - Rbx(pi2)) < 1e-8), 'y(90) * x(90) * z(-90) == x(90).'
    # assert np.all(np.abs(Rbz(pi2) * Rbx(pi2) * Rby(pi2) - Rbx(pi2)) < 1e-8), 'z(90) * x(90) * y(90) == x(90).'

    # Test that fields with a principal direction aligned with the z-axis
    # have a Y_4_0 component equal to np.sqrt(7/12), as proven for Case 1
    # in section 3.4 (Constraints) of Sokolov & Ray (arxiv 1507.03351).
    residual = atilde().transpose() * Rbz(np.pi/6) * np.matrix([0, 0, 0, 0, 1, 0, 0, 0, 0]).transpose() - np.sqrt(7/12)
    assert np.all(np.abs(residual < 1e-3)), 'Z-aligned frame has improper Y_4_0 coefficient'

    # Test Eb{x,y,z} vs Rb{x,y,z} for small rotations
    smol = np.pi/256
    residualX = Rbx(smol) - np.matrix(np.eye(9)) - smol * Ebx()
    # FIXME: Rby appears to be inverted!!!
    residualY = Rby(-smol) - np.matrix(np.eye(9)) - smol * Eby()
    residualZ = Rbz(smol) - np.matrix(np.eye(9)) - smol * Ebz()
    # np.set_printoptions(precision=3)
    # np.set_printoptions(suppress=True)
    # print(residualY)
    print(smol, np.max(np.abs(residualX)) / smol, np.max(np.abs(residualY)) / smol, np.max(np.abs(residualZ)) / smol)
    assert (np.max(np.abs(residualX)) / smol < 0.15), 'Rbx(small) - I9x9 - small * Ebx() should be o(small)'
    assert (np.max(np.abs(residualY)) / smol < 0.15), 'Rby(small) - I9x9 - small * Eby() should be o(small)'
    assert (np.max(np.abs(residualZ)) / smol < 0.15), 'Rbz(small) - I9x9 - small * Ebz() should be o(small)'

    # Test closestFrame
    print('testing closestFrame does no work for idempotent input')
    f, a = closestFrame(Rbz(0)*atilde())
    assert np.all(np.abs(f - I3) < 1e-8), 'Closest frame to z normal is default frame.'
    assert np.all(np.abs(a - atilde()) < 1e-8), 'Closest harmonic to z normal is atilde.'

    # print('testing closestFrame does some work for rotated input')
    # f, a = closestFrame(Rbx(np.pi/16)*Rby(np.pi/16)*atilde())
    # print(f, '\n', Rx(np.pi/16)*Ry(np.pi/16), '\n', f - Rx(np.pi/16)*Ry(np.pi/16))
    # assert np.all(np.abs(f - Rx(np.pi/16)*Ry(np.pi/16)) < 1e-8), 'Garbage in == garbage out.'

    assert np.all(np.abs(so3[0] - np.eye(3)) < 1e-6), 'First entry in SO(3) should be identity matrix'
    assert np.all(so3[0] == computeSO3Class(Rz(0), Rz(np.pi/6))), 'Small rotation should be congruent to identity matrix'
    assert np.all(so3[3] == computeSO3Class(Rz(0), Rz(np.pi/3))), 'Large-ish Z rotation should not be congruent to identity matrix'

    # Test singularity identification
    assert isSingular((Rx(pi2), Ry(pi2), Rz(pi2))), 'Sequence of rotations not identity is singular'
    assert not isSingular((Rx(pi2), Ry(pi2), Rz(pi2), Ry(-pi2))), 'Sequence of rotations identity is not singular'
    assert not isSingular((Ry(pi2), Rx(pi2), Rz(-pi2), Rx(-pi2))), 'Sequence of rotations identity is not singular'
    assert not isSingular((Rz(pi2), Rx(pi2), Ry(pi2), Rx(-pi2))), 'Sequence of rotations identity is not singular'

    print('Success')
    return True

if __name__ == "__main__":
    test()
