# Frame Fields

![An example frame field](teaser.png)

Frame fields are functions whose domain is Euclidean space (usually R^2 or R^3)
and whose range is tuples of orthonormal unit vectors.
You can think of them as functions that specify a Frenet frame at each point in space.

This plugin provides methods to create frame fields over all space by constraining
a set of points in space to have unit vectors pointing along preferred directions.
